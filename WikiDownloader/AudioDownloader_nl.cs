﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Util;

namespace WikiDownloader
{
    class AudioDownloader_nl : AudioDownloader
    {

        protected override string getLanguage () { return "nl"; }

        protected override ExtractedData ParseAudioPageXML(AudioMetadata audio_file, XmlDocument xmlDoc)
        {
            XmlElement template;
            if ((template = GetTemplateByTitle(xmlDoc, "Dutch spoken article")) != null)
            {
                ExtractedData result = new ExtractedData { article_oldid = -1 };

                foreach (var part in template.GetChildElements("part"))
                {
                    string key = part.GetChildElement("name").GetInnerTextOrNull();
                    string value = part.GetChildElement("value").GetInnerTextOrNull();
                    if (key == null) continue;
                    key = key.ToLowerInvariant().Trim();
                    if (key == "date")
                        result.date_read = value;
                    else if (key == "user_name")
                        result.reader = value;
                    else if (key == "link_to_recorded_version")
                    {
                        Match m;
                        if (value.TryMatch(@"\d{5,}", out m))
                            result.article_oldid = long.Parse(m.Value);
                    }
                }
                return result;
            }
            return  new ExtractedData { article_oldid = -1 };
        }

        protected override ExtractedData ParseArticleXML(long pageid, XmlDocument xmlDoc)
        {
            XmlElement template;
            if ((template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia")) != null ||
                (template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia klein")) != null)
            {
                ExtractedData result = new ExtractedData { article_oldid = -1 };
                result.audio_file_titles = new string[] { "Bestand:"+ template.GetChildElement("part").GetChildElement("value").GetInnerTextOrNull() };
                return result;
            }
            else if (
                (template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia 2")) != null ||
                (template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia klein 2")) != null ||
                (template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia 3")) != null ||
                (template = GetTemplateByTitle(xmlDoc, "Gesproken Wikipedia klein 3")) != null)
            {
                ExtractedData result = new ExtractedData { article_oldid = -1 };
                result.audio_file_titles = template.GetChildElements("part")
                    .Select(part => "Bestand:" + part.GetChildElement("value").GetInnerTextOrNull())
                    .ToArray();
                return result;
            }
            return  new ExtractedData { article_oldid = -1 };
        }
    }
}
