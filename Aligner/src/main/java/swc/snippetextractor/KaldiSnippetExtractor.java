package swc.snippetextractor;

import edu.cmu.sphinx.util.TimeFrame;
import org.jdom2.Document;
import swc.data.SwcXmlUtil;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class KaldiSnippetExtractor extends SnippetExtractor {
    private BufferedWriter outfileText;
    private BufferedWriter outfileSegments;
    private BufferedWriter outfileUttSpk;
    private BufferedWriter outfileWavScp;
    private String articleName;
    private String articleReader;
    private boolean wavScpWritten = false;
    private int readerId;
    private int articleId;
    private List<File> audioFiles;
    public KaldiSnippetExtractor(File audioFile, Document document, File outputDir, String articleName, HashMap<String, Integer> speakerMap, int articleId) throws IOException
    {
        super(audioFile, document, outputDir);
        outfileText = new BufferedWriter(new FileWriter(outputDir.getAbsolutePath() + "/text", true));
        outfileSegments = new BufferedWriter(new FileWriter(outputDir.getAbsolutePath() + "/segments", true));
        outfileUttSpk = new BufferedWriter(new FileWriter(outputDir.getAbsolutePath() + "/utt2spk", true));
        outfileWavScp = new BufferedWriter(new FileWriter(outputDir.getAbsolutePath() + "/wav.scp", true));
        this.articleName = articleName;
        SwcXmlUtil wrapped = new SwcXmlUtil(document);
        articleReader = wrapped.getReader();
        if (!speakerMap.containsKey(articleReader)) {
            BufferedWriter id2spk = new BufferedWriter(new FileWriter(outputDir.getAbsolutePath() + "/id2spk", true));
            id2spk.write(speakerMap.size() + " " + articleReader);
            id2spk.newLine();
            id2spk.close();
            speakerMap.put(articleReader, speakerMap.size());
        }
        readerId = speakerMap.get(articleReader);
        this.articleId = articleId;
        audioFiles = Arrays.asList(audioFile.getParentFile().listFiles(
                (f) -> f.getName().startsWith("audio") && f.getName().endsWith(".ogg")));
        java.util.Collections.sort(audioFiles);
    }

    @Override
    protected void createAudioSnippet(TimeFrame tf, File outputFile) {
        // don't need to generate audio for kaldi output
    }

    @Override
    protected void createAudioSnippet(double start_seconds, double stop_seconds, File output_file) {
        // don't need to generate audio for kaldi output
    }

    @Override
    protected void finishedWritingCallback() {
        try {
            outfileText.close();
            outfileSegments.close();
            outfileUttSpk.close();
            outfileWavScp.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

/*    private Pattern readerLinkRE = Pattern.compile(".*\\[\\[.*\\||:(.*)\\]\\].*");

    private String sanitizedReader() {
        String result = articleReader;
        // if the reader is a link, replace with the name, e,g, [[User:foobar|foobar]] -> foobar
        Matcher m = readerLinkRE.matcher(articleReader);
        if (m.matches())
            result = m.group(1);
        result = result.replaceAll(" ", "_").replaceAll("\\[", "").replaceAll("]", "");
        return result;
    }*/

    @Override
    protected void doWriteGroup(int i, Word[] group_words, String normalized, double start, double stop) throws IOException {
        start = Math.max(start, 0.0);
        // String reader = sanitizedReader();
        // String id = reader + "_" + articleName + "_" + String.format(Locale.US,"%.0f", start*100) + "-" + String.format(Locale.US, "%.0f", stop*100);
		String readerIdStr = String.format("%07d", readerId);
        String sentenceid = new StringBuilder()
                .append(readerIdStr)
                .append("_")
                .append(String.format("%07d", articleId)).toString();
        String uttid = new StringBuilder(sentenceid)
                .append("_")
                .append(String.format(Locale.US, "%.0f", start * 100))
                .append("-")
                .append(String.format(Locale.US, "%.0f", stop * 100)).toString();

        outfileText.write(uttid + " " + normalized);
        outfileText.newLine();

        outfileSegments.write(String.format(Locale.US, "%s %s %.3f %.3f", uttid, sentenceid, start, stop));
        outfileSegments.newLine();

        outfileUttSpk.write(uttid + " " + readerIdStr);
        outfileUttSpk.newLine();

        if (! wavScpWritten) {
            StringBuilder wavGenerator = new StringBuilder("sox");
            for (File f : audioFiles) {
                wavGenerator
                        .append(" /path/to/articles/")
                        .append(f.getParentFile().getName())
                        .append("/")
                        .append(f.getName());
            }
            wavGenerator.append(" --bits 16 --endian little --channels 1 --encoding signed-integer --rate 16000 -t wav - |");
            outfileWavScp.write(sentenceid + " " + wavGenerator);
            outfileWavScp.newLine();
            wavScpWritten = true;
        }
    }
}
