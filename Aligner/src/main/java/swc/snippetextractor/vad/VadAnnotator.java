package swc.snippetextractor.vad;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import edu.cmu.sphinx.frontend.Data;
import edu.cmu.sphinx.frontend.FrontEnd;
import edu.cmu.sphinx.frontend.Signal;
import edu.cmu.sphinx.frontend.endpoint.SpeechEndSignal;
import edu.cmu.sphinx.frontend.endpoint.SpeechStartSignal;
import edu.cmu.sphinx.frontend.util.StreamDataSource;
import edu.cmu.sphinx.util.TimeFrame;
import edu.cmu.sphinx.util.props.ConfigurationManager;

public class VadAnnotator
{

    public static void main(String... args) throws UnsupportedAudioFileException, IOException
    {
        if(args.length == 0)
        {
            System.err.println("Expect one argument: audiofile.wav");
            System.exit(-1);
        }
        for(TimeFrame frame: annotate(new File(args[0]).toURI().toURL()))
        {
            System.out.print(String.format("%.3f\t%.3f\n", frame.getStart()/1000.0, frame.getEnd()/1000.0));
        }
    }

    /**
     * Returns a List of TimeFrames. A TimeFrame has start, end and duration.
     * The TimeFrames are utterances found in the audio.
     * @param audioUrl
     * @return
     * @throws UnsupportedAudioFileException
     * @throws IOException
     */
    public static List<TimeFrame> annotate(URL audioUrl) throws UnsupportedAudioFileException, IOException
    {
        List<TimeFrame> frames = new ArrayList<TimeFrame>();
        ConfigurationManager cm = new ConfigurationManager(VadAnnotator.class.getResource("/vad-config.xml"));
        StreamDataSource sds = (StreamDataSource) cm.lookup("streamDataSource");
        sds.setInputStream(AudioSystem.getAudioInputStream(audioUrl));
        FrontEnd fe = (FrontEnd) cm.lookup("endpointing");
        Data d;
        boolean inSpeech = false;
        long startTime = -1;
        while ((d = fe.getData()) != null)
        {
            if (d instanceof Signal)
            {
                Signal s = (Signal) d;
                if (!inSpeech)
                {
                    if (s instanceof SpeechStartSignal)
                    {
                        inSpeech = true;
                        startTime = s.getTime();
                    }
                } else
                {
                    if (s instanceof SpeechEndSignal)
                    {
                        inSpeech = false;
                        frames.add(new TimeFrame(startTime, s.getTime()));
                    }
                }
            }
        }
        return frames;
    }
}
