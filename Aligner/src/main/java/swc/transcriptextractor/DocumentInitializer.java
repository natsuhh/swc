package swc.transcriptextractor;

import org.jdom2.Document;
import org.jdom2.Element;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swc.data.SwcXmlUtil;
import swc.io.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by felix on 13/02/17.
 */
public class DocumentInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DocumentInitializer.class);

    public class MetaInformationMissingException extends Exception {

        public MetaInformationMissingException() {
            super("Important information in the info.json is missing.");
        }

        public MetaInformationMissingException(String missingkey) {
            super("Key " + missingkey + " is missing from the Json File.");
        }
    }

    private final File articleDirectory;
    private final SwcXmlUtil sxu = new SwcXmlUtil();

    public DocumentInitializer(File articleDirectory) {

        this.articleDirectory = articleDirectory;

    }

    /**
     * Adds static information to the document, about the schema, creator, publisher and license.
     */
    public void addConstants() {
        sxu.addMetaLink("DC.conformsto", "http://nats.gitlab.io/swc/schema/swc-1.0.rnc");  // constant  TODO
        sxu.addMetaProp("DC.creator", "Spoken Wikipedia Corpus Collection Software");      // constant
        sxu.addMetaProp("DC.publisher", "Universität Hamburg");                            // constant  all of these could go in a config file
        sxu.addMetaLink("DC.reference", "http://nbn-resolving.de/urn:nbn:de:gbv:18-228-7-2209");                    // constant
        sxu.addMetaProp("DC.type", "dataset");                                             // constant
        sxu.addMetaProp("DC.license", "CC-BY-SA");                                         // constant
    }

    /**
     * Reads the json info file in the article directory and adds the metadata
     * to the document.
     */
    public void readJsonInfo() throws IOException, MetaInformationMissingException {
        String infoJsonString = FileUtil.ReadAllText(new File(articleDirectory, "info.json"));
        JSONObject infoJson = new JSONObject(infoJsonString);

        JSONObject articleInfo = infoJson.optJSONObject("article");
        JSONObject articleParsedInfo = infoJson.optJSONObject("article_parsed");

        if (articleInfo == null || articleParsedInfo == null)
            throw new MetaInformationMissingException();

        // The addMetaXXXX-methods don't add anything if the value given is null.

        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.title", "article.title", infoJson, null, false);
        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.language", "article.language", infoJson, null, false);
        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.identifier", "article.identifier", infoJson, null, false);
        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.date.read", "article_parsed.date_read", infoJson, null, true);
        addEntry(SwcXmlUtil.META_LINK_TAG, "DC.source", "article.canonicalurl", infoJson, null, false);
        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.source.wikiID", "article.pageid", infoJson, null, false);
        addEntry(SwcXmlUtil.META_PROP_TAG, "DC.source.revision", "article.revision", infoJson, null, true);
        addEntry(SwcXmlUtil.META_LINK_TAG, "DC.source.text", "article_parsed.oldURL", infoJson, null, true);

        // audio file information
        JSONArray audioFiles = infoJson.optJSONArray("audio_files");
        if (audioFiles == null)
            throw new MetaInformationMissingException();
        int audioFileNumber = 1;
        for (Object audioFile : audioFiles) {
            JSONObject aFile = (JSONObject)audioFile;
            String groupId = "audio" + Integer.toString(audioFileNumber++);
            addEntry(SwcXmlUtil.META_LINK_TAG, "DC.source.audio", "downloadurl", aFile, groupId, false);
            addEntry(SwcXmlUtil.META_PROP_TAG, "DC.source.audio.offset", "offset", aFile, groupId, false);
            addEntry(SwcXmlUtil.META_LINK_TAG, "DC.source.audio.page", "metadataURL", aFile, groupId, true);
            addEntry(SwcXmlUtil.META_LINK_TAG, "DC.source.audio.date", "timestamp", aFile, groupId, true);
        }


        // non-DC properties
        sxu.addMetaProp("reader.name", extractReader(infoJson));
    }

    private static class ReaderInfo {
        String name;
        int quality;

        ReaderInfo(String name, int quality) {
            this.name = name;
            this.quality = quality;
        }
    }

    private static String extractReader(JSONObject infoJson) {
        List<ReaderInfo> infos = new ArrayList<>();

        if (infoJson.optJSONObject("article_parsed") != null &&
                !infoJson.getJSONObject("article_parsed").isNull("reader")) {
            String name = infoJson.getJSONObject("article_parsed").getString("reader");
            int quality = infoJson.getJSONObject("article_parsed").getInt("oldid_quality"); // quality should always be there
            infos.add(new ReaderInfo(name, quality));
        }

        if (infoJson.optJSONObject("audio_file_parsed") != null &&
                !infoJson.getJSONObject("audio_file_parsed").isNull("reader")) {
            String name = infoJson.getJSONObject("audio_file_parsed").getString("reader");
            int quality = infoJson.getJSONObject("audio_file_parsed").getInt("oldid_quality"); // quality should always be there
            infos.add(new ReaderInfo(name, quality));
        }

        if (infos.isEmpty())
            return null;

        // oldid_quality is
        //   1 if the oldid was guessed by looking at the revision of the date given for the change
        //   2 if the oldid was parsed directly from the template

        infos.sort((i1, i2) -> Integer.compare(i1.quality, i2.quality));

        return infos.get(infos.size() - 1).name; // highest oldID quality -> best
    }

    /**
     * Adds a meta entry to the document.
     * @param type  either SwcXmlUtil.META_LINK_TAG or SwcXmlUtil.META_PROP_TAG
     * @param targetKey  DC.title, DC.source.audio.page, ...
     * @param sourceKey  article.title, article_parsed.date_read, ...
     * @param infoJson  the root object for the path given in sourceKey
     * @param group  a group identifier or null
     * @param optional  true, if the sourceKey might be missing in infoJson.  If false, an exception is thrown
     * @throws MetaInformationMissingException  if optional == false and sourceKey is missing in infoJson
     */
    private void addEntry(String type, String targetKey, String sourceKey, JSONObject infoJson,
                          String group, boolean optional) throws MetaInformationMissingException{
        List<String> path = new ArrayList<>(Arrays.asList(sourceKey.split("\\.")));
        Object value = infoJson;
        while (path.size() > 0) {
            value = ((JSONObject)value).opt(path.get(0));
            path.remove(0);
            if (value == null) {
                if (optional) {
                    logger.warn(sourceKey + " in info.json is null.");
                    return;
                }
                else
                    throw new MetaInformationMissingException(sourceKey);
            }
        }
        String valStr = value.toString();
        sxu.addMetaEntry(type, targetKey, valStr, group);
    }

    public Document getInitialDocument() {
        return sxu.getDocument();
    }
}
