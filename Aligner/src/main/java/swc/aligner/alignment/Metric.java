package swc.aligner.alignment;

public interface Metric<T>
{
    /**
     * Returns some value >= 0 for the given item
     * @param item the item to be masured
     * @return a user defined value >= 0
     */
    public int measure(T item);
}
