package swc.aligner.alignment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SequenceAligner<T>
{
    public SequenceAligner(List<T> reference)
    {
        _reference  = reference;
        _positions = new HashMap<>();
        for(int i = 0;i<reference.size();i++)
        {
            T item = reference.get(i);
            List<Integer> item_positions = _positions.get(item);
            if(item_positions == null)
            {
                item_positions = new ArrayList<Integer>();
                _positions.put(item, item_positions);
            }
            item_positions.add(i);
        }
    }
    
    private HashMap<T,List<Integer>> _positions;
    private List<T> _reference;
    
    private static class Prefix extends IndexPair
    {
        public Prefix(int a, int b, Prefix prefix)
        {
            super(a,b);
            this.prefix = prefix;
            if(prefix!= null)
                this.sequence_length = prefix.sequence_length+1;
            else
                this.sequence_length = 1;
        }
        
        public int sequence_length;
        public Prefix prefix;
    }
    
    public IndexPair[] getLongestCommonSubsequence(int ref_start, int ref_end, List<T> seq)
    {
        return getLongestCommonSubsequence(ref_start, ref_end, seq, 0, seq.size());
    }
    
    public IndexPair[] getLongestCommonSubsequence(int ref_start, int ref_end, List<T> seq, int seq_start, int seq_end)
    {
        Prefix best_prefix = null;
        TreeMap<Integer, Prefix> prefix_by_reference_index = new TreeMap<>();
        for(int seq_pos=seq_start; seq_pos<seq_end; seq_pos++)
        {
            T item = seq.get(seq_pos);
            List<Integer> item_positions = _positions.get(item);
            if(item_positions == null)
                continue;
            
            position_loop: for(int ref_pos:item_positions)
            {
                if(ref_pos < ref_start || ref_pos >= ref_end)
                    continue;//out of range
                Entry<Integer, Prefix> previous_entry = prefix_by_reference_index.floorEntry(ref_pos-1);
                Prefix prefix = null;
                if(previous_entry != null)
                {
                    prefix = previous_entry.getValue();
                    if(prefix.getRight() >= seq_pos)
                        continue;
                }
                
                Prefix new_prefix = new Prefix(ref_pos, seq_pos, prefix);
                //clean list
                Entry<Integer, Prefix> entry;
                while((entry = prefix_by_reference_index.ceilingEntry(ref_pos)) != null)
                {
                    if(entry.getKey() == ref_pos && entry.getValue().sequence_length >= new_prefix.sequence_length)
                        continue position_loop;//something better already here --> skip insertion of new entry
                    else if(entry.getValue().sequence_length <= new_prefix.sequence_length)
                        prefix_by_reference_index.remove(entry.getKey());//next entry is worse/shorter --> remove it
                    else
                        break;//next entry is longer/better --> leave it there
                }
                //insert new
                prefix_by_reference_index.put(ref_pos, new_prefix);
                //the best prefix is also always the longest subsequence
                if(best_prefix == null || new_prefix.sequence_length > best_prefix.sequence_length)
                    best_prefix = new_prefix;
            }
        }
        if(best_prefix == null)
            return new IndexPair[0];
        IndexPair[] result = new IndexPair[best_prefix.sequence_length];
        Prefix p = best_prefix;
        while(p != null)
        {
            result[p.sequence_length-1] = p;
            p = p.prefix;
        }
        return result;
    }
    
    public static class CommonSubstring extends IndexPair
    {
        public CommonSubstring(int left, int right, int length, int score)
        {
            super(left,right);
            this._length = length;
            this._score = score;
        }
        
        private int _length;
        private int _score;
        
        public int getLength()
        {
            return _length;
        }
        
        public int getScore()
        {
            return _score;
        }

        public int getLeftEnd()
        {
            return getLeft() + _length;
        }
        
        public int getRightEnd()
        {
            return getRight() + _length;
        }
    }

    public CommonSubstring getLongestCommonSubstring(int ref_start, int ref_end, List<T> seq)
    {
        return getLongestCommonSubstring(ref_start, ref_end, seq, 0, seq.size(), null);
    }
    
    public CommonSubstring getLongestCommonSubstring(int ref_start, int ref_end, List<T> seq, Metric<T> metric)
    {
        return getLongestCommonSubstring(ref_start, ref_end, seq, 0, seq.size(), metric);
    }

    public CommonSubstring getLongestCommonSubstring(int ref_start, int ref_end, List<T> seq, int seq_start, int seq_end, Metric<T> metric)
    {
        CommonSubstring longest = null;
        for (int seq_pos = seq_start; seq_pos < seq_end; seq_pos++)
        {
            T item = seq.get(seq_pos);
            List<Integer> item_positions = _positions.get(item);
            if (item_positions == null)
                continue;

            for (int ref_pos : item_positions)
            {
                if (ref_pos < ref_start || ref_pos >= ref_end)
                    continue;// out of range
                
                //found the start?
                if(ref_pos == ref_start || seq_pos == seq_start || !equals(_reference.get(ref_pos-1), seq.get(seq_pos-1)))
                {
                    //then count...
                    int n = 0;
                    int score = 0;
                    while(ref_pos+n < ref_end && seq_pos+n < seq_end && equals(_reference.get(ref_pos+n), seq.get(seq_pos+n)))
                    {
                        score += (metric == null)? 1: metric.measure(seq.get(seq_pos+n));
                        n++;
                    }
                    
                    //and compare to best
                    if(longest == null || longest.getScore() < score)
                        longest = new CommonSubstring(ref_pos, seq_pos, n, score);
                }
            }
        }
        return longest;
    }

    private boolean equals(T v1, T v2)
    {
        if(v1 == v2)
            return true;
        if(v1 == null)
            return v2.equals(v1);
        return v1.equals(v2);
    }

    public List<T> getReference()
    {
        return _reference;
    }
}
