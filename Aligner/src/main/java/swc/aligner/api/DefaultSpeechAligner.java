/*
 * Copyright 2014 Florian Stegen
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 */

package swc.aligner.api;

import edu.cmu.sphinx.decoder.adaptation.ClusteredDensityFileData;
import edu.cmu.sphinx.decoder.adaptation.Stats;
import edu.cmu.sphinx.decoder.adaptation.Transform;
import edu.cmu.sphinx.decoder.search.Token;
import edu.cmu.sphinx.frontend.Data;
import edu.cmu.sphinx.frontend.DoubleData;
import edu.cmu.sphinx.frontend.FloatData;
import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.result.WordResult;
import edu.cmu.sphinx.util.TimeFrame;
import swc.aligner.alignment.Metric;
import swc.aligner.alignment.SequenceAligner;
import swc.aligner.alignment.SequenceAligner.CommonSubstring;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

/**
 * @author Alexander Solovets
 */
public class DefaultSpeechAligner extends SpeechAligner {
    
    private boolean use_speaker_adaption = false;
    private boolean use_allphone_alignment = false;
    private PhoneSpeechAligner phone_aligner;
    
    /**
     * TODO: fill
     */
    public DefaultSpeechAligner(String amPath, String dictPath, String g2pPath, boolean use_allphone_alignment)
            throws MalformedURLException, IOException {
        super(amPath, dictPath, g2pPath);
        this.use_allphone_alignment = use_allphone_alignment;
        if(use_allphone_alignment)
            phone_aligner = new PhoneSpeechAligner(amPath, dictPath, g2pPath);
    }

    private static class AlignmentRange
    {
        public AlignmentRange(TimeFrame time_frame, int text_start, int text_end, int recursion_level)
        {
            this.time_frame = time_frame;
            this.text_start = text_start;
            this.text_end = text_end;
            this.recursion_level = recursion_level;
        }
        
        public TimeFrame time_frame;
        public int text_start;
        public int text_end;
        public int recursion_level;
    }
    
    private static class WordWithTiming
    {
        public WordWithTiming(Word word, List<Token> word_tokens, @SuppressWarnings("unused") boolean keep_tokens)
        {
            this.word = word;
            long start = Long.MAX_VALUE;
            long end = 0;
            for(Token t:word_tokens)
            {
                Data data = t.getData();
                long time;
                if(data instanceof FloatData)
                {
                    FloatData floatData = (FloatData) data;
                    time = floatData.getFirstSampleNumber()*1000/floatData.getSampleRate();
                }
                else if(data instanceof DoubleData)
                {
                    DoubleData doubleData = (DoubleData) data;
                    time = doubleData.getFirstSampleNumber()*1000/doubleData.getSampleRate();
                }
                else
                     continue;
                
                if(time < start)
                    start = time;
                if(time+10 > end)
                    end = time+10;
            }
            time_frame = new TimeFrame(start, end);
        }
        public TimeFrame time_frame;
        public Word word;
        
        @Override
        public String toString()
        {
            return word.getSpelling();
        }
    }
    
    /**
     * TOOD: fill
     */
    @Override
    public List<WordResult> align(URL audioUrl, List<String> transcript)
            throws Exception {
        List<WordResult> results = Arrays.asList(new WordResult[transcript.size()]);
        edu.cmu.sphinx.linguist.dictionary.Dictionary dictionary = context.getInstance(edu.cmu.sphinx.linguist.dictionary.Dictionary.class);
        dictionary.allocate();
        for (int i = 0; i < transcript.size(); i++) {
            results.set(i, new WordResult(dictionary.getWord(transcript.get(i)), null, -1, -1));
        }

        SequenceAligner<String> text_aligner = new SequenceAligner<>(transcript);
        Queue<AlignmentRange> ranges = new ArrayDeque<>();
        Queue<AlignmentRange> ranges_next_iteration = new ArrayDeque<>();

        // add the full range to start with
        ranges.add(new AlignmentRange(TimeFrame.INFINITE, 0, transcript.size(), 1));

        // stuff for speaker-adaptation (if set through use_speaker_adaptation)
        int num_speakers = 1;
        ClusteredDensityFileData clusters = new ClusteredDensityFileData(context.getLoader(), num_speakers);
        Stats stats = new Stats(context.getLoader(), clusters);
        
        for (int iteration = 0; iteration < (use_allphone_alignment?1:3); ++iteration) {
        	logger.info("Now starting pass: " + (iteration+1));
            if (iteration == 1) {
                context.setLocalProperty("decoder->searchManager", "alignerSearchManager");
            } else if (iteration == 2) {
            	logger.info("Final pass in grammar mode");
            }
            
            // this is the main algorithm: try to find stretches within current range and add remaining sub-ranges to the queue
            while (!ranges.isEmpty()) {
                AlignmentRange range = ranges.poll();
                
                // skip frames of < 0.5 seconds and skip ranges without text
                if(range.time_frame.length() <= 5 || range.text_start >= range.text_end)
                    continue;
                
                List<String> text = transcript.subList(range.text_start, range.text_end);
                logger.info("In iteration " + (iteration+1) + " aligning frame " + range.time_frame + " to text " + text + " range " + range.time_frame + "at level " + range.recursion_level);

                if (iteration < 2) { // set LM to text unless we're in grammar mode
                    languageModel.setText(text);
                }

                if (use_speaker_adaption) {
                    Transform trans = null;
                    try {
                        trans = stats.createTransform();
                    } catch (Exception ex){
                        logger.log(Level.WARNING, ex.getMessage());
                    }
                    if (trans != null)
                        context.getLoader().update(trans, clusters);
                }

                recognizer.allocate();

                if (iteration >= 2) { // grammar mode for final pass
                    grammar.setWords(text);
                }

                context.setSpeechSource(audioUrl.openStream(), range.time_frame);

                List<WordWithTiming> recognized_words = new ArrayList<>();
                List<String> recognized_word_strings = new ArrayList<>();
                
                Result result;
                while (null != (result = recognizer.recognize())) {
                     for(WordWithTiming recognized_word: getWords(result, use_speaker_adaption))
                     {
                         if(recognized_word.word.isFiller())
                             continue;
                         recognized_words.add(recognized_word);
                         recognized_word_strings.add(recognized_word.word.getSpelling());
                     }
                }

                logger.info("found: "+recognized_word_strings);
                
                List<CommonSubstring> matches = new ArrayList<>();
                List<CorrespondingRange> gaps = new ArrayList<>();
                findCommonSubstrings(recognized_word_strings, text_aligner, range.text_start, range.text_end, matches, gaps);
                
                //process results
                for (CommonSubstring match: matches)
                    for(int i=0;i<match.getLength(); i++)
                    {
                        WordWithTiming word = recognized_words.get(match.getRight()+i);
                        WordResult word_result = new WordResult(word.word, word.time_frame, 0, 0);
                        // store the result that we want to keep
                        results.set(match.getLeft()+i, word_result);
//                        if(use_speaker_adaption)
//                        {
//                            for(Token t: word.tokens)
//                                stats.collect(t);
//                        }
                        
                        logger.info("Word accepted in level "+ range.recursion_level + ": " +word.word.getSpelling() + " timed: " + word.time_frame);
                    }
                
                //process missing data
                if (matches.size() > 0) {
                    for (CorrespondingRange gap : gaps) {
                        long start, end;
                        if(gap.right_start > 0)
                            start = recognized_words.get(gap.right_start-1).time_frame.getEnd();//starts after previous word
                        else
                            start = range.time_frame.getStart();//no previous word => starts at start
                        
                        if(gap.right_end < recognized_words.size())
                            end = recognized_words.get(gap.right_end).time_frame.getStart();//ends before next word
                        else
                            end = range.time_frame.getEnd();//no following word => ends at end
                        
                        ranges.add(new AlignmentRange(new TimeFrame(start, end), gap.left_start, gap.left_end, range.recursion_level +1));
                    }
                } else {
                    if (range.text_end - range.text_start < 100 && range.time_frame.length() < 100 * 100)  // 100 words and 100 seconds (* 100 frames per second)
                		ranges_next_iteration.add(range);// nothing found -> maybe more luck next iteration?
                }
                recognizer.deallocate();
            }
            ranges = ranges_next_iteration;
            ranges_next_iteration = new ArrayDeque<>();
        }
        
        if (use_allphone_alignment) {
            phonealignment(audioUrl, transcript, ranges, results);
        }
        
        return results;
    }

    private List<WordWithTiming> getWords(Result result, boolean keep_tokens)
    {
        List<WordWithTiming> words = new ArrayList<DefaultSpeechAligner.WordWithTiming>();
        
        Word word = null;
        List<Token> word_tokens = result.getWordTokenFirst() ? new ArrayList<Token>() : null;
        for(Token t = result.getBestToken(); t != null; t= t.getPredecessor())
        {
            if(t.getWord() != null) // i.e., this is a word!
            {
                if (result.getWordTokenFirst())
                    word = t.getWord();
                if(word != null && !word.isSentenceEndWord())
                    words.add(new WordWithTiming(word, word_tokens, keep_tokens));
                if (!result.getWordTokenFirst())
                    word = t.getWord();
                word_tokens = new ArrayList<Token>();
            }
            if(word_tokens != null)
                word_tokens.add(t);
        }
        if(word != null)
        {
            //Collections.reverse(word_tokens);
            words.add(new WordWithTiming(word, word_tokens, keep_tokens));
        }
        Collections.reverse(words);
        return words;
    }
    
    private static class CorrespondingRange
    {
        public CorrespondingRange(int left_start, int left_end, int right_start, int right_end)
        {
            this.left_start = left_start;
            this.left_end = left_end;
            this.right_start = right_start;
            this.right_end = right_end;
        }
        
        public int left_start, left_end;
        public int right_start, right_end;
    }
    
    private static final Metric<String> stringLengthMetric = new Metric<String>()
    {
        @Override
        public int measure(String item)
        {
            if(item == null)
                return 0;
            return item.length();
        }
    };

    private void findCommonSubstrings(List<String> recognized_word_strings,
            SequenceAligner<String> text_aligner, int text_start, int text_end,
            List<CommonSubstring> results, List<CorrespondingRange> gaps)
    {
        
        Stack<CorrespondingRange> ranges = new Stack<CorrespondingRange>();
        ranges.push(new CorrespondingRange(text_start, text_end, 0, recognized_word_strings.size()));
        
        while(!ranges.isEmpty())
        {
            CorrespondingRange range = ranges.pop();
            boolean lEmpty = range.left_start >= range.left_end;
            boolean rEmpty = range.right_start >= range.right_end;
            if(lEmpty && rEmpty)
                continue;//empty
            if(lEmpty || rEmpty)
            {
                gaps.add(range);
                continue;
            }
            
            CommonSubstring longest_in_range = text_aligner.getLongestCommonSubstring(range.left_start, range.left_end,
                    recognized_word_strings, range.right_start, range.right_end, stringLengthMetric);
            
            //required length is 30 chars or 1/3 the remaining text, or the entire remaining text when using allphone alignment
            int required_length = 30;
            int denom = 3;
            if(use_allphone_alignment)
            {
                required_length = 40;
                denom = 1;
            }
            int ref_length = 0;
            for(int i = range.left_start; i < range.left_end; i++)
            {
                ref_length += stringLengthMetric.measure(text_aligner.getReference().get(i));
                if(ref_length/denom >= required_length)
                    break;
            }
            if(ref_length/denom < required_length)
                required_length = ref_length/denom;
            
            if(longest_in_range != null && longest_in_range.getScore() >= required_length)
            {
                ranges.add(new CorrespondingRange(
                        longest_in_range.getLeftEnd(), range.left_end,
                        longest_in_range.getRightEnd(), range.right_end));
                ranges.add(new CorrespondingRange(
                        range.left_start, longest_in_range.getLeft(),
                        range.right_start, longest_in_range.getRight()));
                results.add(longest_in_range);
            }
            else
            {
                gaps.add(range);
            }
        }
    }
    

    private void phonealignment(URL audioUrl, List<String> transcript, Queue<AlignmentRange> ranges, List<WordResult> results) throws Exception
    {
        for(AlignmentRange range : ranges)
        {
            int text_start = range.text_start;
            long audio_start = range.time_frame.getStart();
            if(text_start > 0 && results.get(text_start-1).getTimeFrame() != null)
            {
                audio_start = results.get(--text_start).getTimeFrame().getStart();
            }
            int text_end = range.text_end;
            long audio_end = range.time_frame.getEnd();
            if(text_end + 1 < transcript.size() && results.get(text_end+1).getTimeFrame() != null)
            {
                audio_end = results.get(++text_end).getTimeFrame().getEnd();
            }
            List<WordResult> results_in_range = phone_aligner.align(audioUrl, new TimeFrame(audio_start, audio_end), transcript.subList(text_start, text_end));
            
            //List<WordResult> results_in_range = phone_aligner.align(audioUrl, range.time_frame, transcript.subList(range.text_start, range.text_end));
            for(int i=0;i<results_in_range.size();i++)
            {
                WordResult r = results_in_range.get(i);
                if(r.getTimeFrame() != null)
                    results.set(text_start + i, r);
            }
        }
    }
}
