package swc.aligner.api;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.Context;
import edu.cmu.sphinx.linguist.language.grammar.AlignerGrammar;
import edu.cmu.sphinx.linguist.language.ngram.DynamicTrigramModel;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.WordResult;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import swc.data.SwcXmlUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class SpeechAligner
{

    protected final Logger logger = Logger.getLogger(getClass().getSimpleName());

    public abstract List<WordResult> align(URL audioUrl, List<String> transcript) throws Exception;

    protected final Context context;
    protected final Recognizer recognizer;
    protected final AlignerGrammar grammar;
    protected final DynamicTrigramModel languageModel;

    public SpeechAligner(String amPath, String dictPath, String g2pPath)
            throws MalformedURLException, IOException
    {
        Configuration configuration = new Configuration();
        configuration.setAcousticModelPath(amPath);
        configuration.setDictionaryPath(dictPath);

        context = new Context(configuration);
        if (g2pPath != null) {
            context.setLocalProperty("dictionary->g2pModelPath", g2pPath);
            context.setLocalProperty("dictionary->g2pMaxPron", "2");
        }
        context.setLocalProperty("lexTreeLinguist->languageModel", "dynamicTrigramModel");
        
        onContextCreated();
        
        recognizer = context.getInstance(Recognizer.class);
        grammar = context.getInstance(AlignerGrammar.class);
        languageModel = context.getInstance(DynamicTrigramModel.class);
    }

    protected void onContextCreated()
    {
    }

    public void addAlignment(Document doc, URL audioUrl) throws Exception {
        SwcXmlUtil sxu = new SwcXmlUtil(doc);
        List<Element> tokens = sxu.getNormalizedTokens();
        List<String> strings = tokens.stream().map(t -> t.getAttributeValue(sxu.PRONUNCIATION_ATTR)).collect(Collectors.toList());
        List<WordResult> aligned = align(audioUrl, strings);
        IntStream.range(0, tokens.size())
                .forEach(i -> sxu.setTiming(tokens.get(i), aligned.get(i).getTimeFrame()));
    }
}