import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.junit.Test;

import swc.data.SwcXmlUtil;

public class SelectiveFlattenTest {
  private static String TEST_INPUT = 
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
+ "<p><ignored>"
+ "  <ignored><t>[</t></ignored>"
+ "  <ignored><ignored><ignored></ignored><t>Bearbeiten<n pronounciation=\"Bearbeiten\" /></t></ignored></ignored>"
+ "  <ignored><t><ignored>|<n pronounciation=\"|\" /></ignored></t></ignored>"
+ "  <t>Quelltext<n pronounciation=\"Quelltext\" /></t>"
+ "  <t>bearbeiten<n pronounciation=\"bearbeiten\" /></t>"
+ "  <ignored><t>]</t></ignored>"
+ "</ignored></p>\r\n";
/* p ignored1
 * 		ignored2 t
  		ignored3 ignored4 ignored5() t n
  		ignored6 t ignored7 n
  		t n
  		t n
  		ignored8 t
 */
  
  private static String TEST_OUTPUT = 
		  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				  + "<p><ignored>"
				  + "  <t>[</t>"
				  + "  <t>Bearbeiten<n pronounciation=\"Bearbeiten\" /></t>"
				  + "  <t>|<n pronounciation=\"|\" /></t>"
				  + "  <t>Quelltext<n pronounciation=\"Quelltext\" /></t>"
				  + "  <t>bearbeiten<n pronounciation=\"bearbeiten\" /></t>"
				  + "  <t>]</t>"
				  + "</ignored></p>\r\n";
  
  @Test
  public void testFlatten() throws JDOMException, IOException {
	  System.out.println(TEST_OUTPUT);
	  Document doc = new SAXBuilder().build(new StringReader(TEST_INPUT));
	  SwcXmlUtil.flattenNestedTag(doc.getRootElement(), SwcXmlUtil.IGNORE_TAG);
	  System.out.println(new XMLOutputter(Format.getPrettyFormat()).outputString(doc));
	  assertEquals(TEST_OUTPUT, new XMLOutputter(Format.getRawFormat()).outputString(doc));
  }
  
  
}