using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Util
{
	public class FastMatch
	{
		class Node
		{
			public SortedDictionary<char, Node> dic = new SortedDictionary<char, Node> ();
			public bool end = false;
		}

		public static string GetExpression (IEnumerable<string> strings)
		{
			var root = new Node ();
			foreach (string str in strings) {
				var b = root;
				foreach (char c in str) {
					Node d;
					if (b.dic.TryGetValue (c, out d))
						b = d;
					else
						b = b.dic [c] = new Node ();
				}
				b.end = true;
			}

			return GetExpression (root);
		}

		static string GetExpression (Node node)
		{
			if (node.dic.Count == 0)
				return "";
			var poss = node.dic.Select (kvp => Regex.Escape (kvp.Key.ToString ()) + GetExpression (kvp.Value)).ToList ();
			if (node.end)
				poss.Add ("");
			if (poss.Count == 0)
				return "";
			if (poss.Count == 1)
				return poss.Single ();
			poss.Sort ((a,b) => {
				int result = -(a ?? "").Length.CompareTo ((b ?? "").Length);
				if (result != 0)
					return result;
				return a.CompareTo (b);
			});
			return "(?:" + string.Join ("|", poss) + ")";
		}
	}
}