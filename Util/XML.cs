﻿using System.Collections.Generic;
using System.Xml;

namespace Util
{
    public static class XML
    {
        public static XmlElement GetChildElement(this XmlElement el, string name)
        {
            var child = el.FirstChild;
            while(child != null)
            {
                if (child.Name == name && child is XmlElement)
                    return (XmlElement)child;
                child = child.NextSibling;
            }
            return null;
        }

        public static IEnumerable<XmlElement> GetChildElements(this XmlElement el, string name)
        {
            var child = el.FirstChild;
            while (child != null)
            {
                if (child.Name == name && child is XmlElement)
                    yield return (XmlElement)child;
                child = child.NextSibling;
            }
        }

        public static bool HasChildElement(this XmlElement el, string name)
        {
            return GetChildElement(el, name) != null;
        }

        public static string GetInnerTextOrNull(this XmlElement el, bool nullIfEmpty = false)
        {
            if (el == null)
                return null;
            string result = el.InnerText;
            if (nullIfEmpty && string.IsNullOrWhiteSpace(result))
                return null;
            return result.Trim();
        }

        public static IEnumerable<XmlNode> EnumerateTree(this XmlNode el)
        {
            while (el != null)
            {
                yield return el;
                if (el.FirstChild != null)
                {
                    el = el.FirstChild;
                }
                else
                {
                    while (el.NextSibling == null)
                    {
                        el = el.ParentNode;
                        if (el == null)
                            yield break;
                    }
                    el = el.NextSibling;
                }
            }
        }
    }
}