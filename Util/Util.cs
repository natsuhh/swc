﻿using System.Text.RegularExpressions;

namespace Util
{
    public static class Strings
    {
        public static bool TryMatch(this string str, string pattern, out Match match)
        {
            match = Regex.Match(str, pattern, RegexOptions.Singleline);
            return match.Success;
        }

        public static string GetGroupValue(this Match match, int i)
        {
            var groups = match.Groups;
            if (groups.Count <= i)
                return null;
            var group = groups[i];
            if (!group.Success)
                return null;
            return group.Value;
        }

        public static string TrimOrNull(this string str)
        {
            if (str == null)
                return null;
            return str.Trim();
        }
    }
}
