#!/usr/bin/perl -I /informatik2/nats/home/baumann/perl5/lib/perl5
use strict;
use warnings;
use utf8;
#use encoding "utf8";
use Encode qw/encode/;
binmode(STDIN, "utf8");
binmode(STDOUT, "utf8");
binmode(STDERR, "utf8");
use JSON;
use URI::Escape;

#analyze alignment quality of the marker sentence 
# "Sie hören den Artikel XXXX, aus Wikipedia, der freien Enzyklopädie."
#in align.data.json files
my @leadingPart = ("Sie ", "hören ", "den ", "Artikel ");
my @trailingPart = (", ", "aus ", "Wikipedia", ", ", "der ", "freien ", "Enzyklopädie", ". ");

my @files = @ARGV;
FILE: foreach my $file (@files) {
	#my @articleName = guessArticleName($file);
	my $json = alignmentJson($file);
	my @articleNameTokens = getArticleTokens($json);
#print join " ", @articleNameTokens;
#print "\n";
	my $startLeader = 0;
	my $endLeader = 3;
	my $startTrailer = $endLeader + 1 + scalar @articleNameTokens;
	my $endTrailer = $startTrailer + 7;
	my $timeable = 0;
	my $withTiming = 0;
	for (my $i = 0; $i <= $endTrailer; $i++) {
		my $word = @{$json->{"words"}}[$i];
		$timeable++ if ($word->{"normalized"} ne "");
		$withTiming++ if (defined $word->{"start"});
	}
	my $startLeaderTime = @{$json->{"words"}}[$startLeader]->{"start"};
#	my $endLeaderTime = @{$json->{"words"}}[$endLeader]->{"stop"};
	my $endLeaderTime = @{$json->{"words"}}[$endLeader+1]->{"start"}; # version that includes potential silence between words
#	my $startTrailerTime = @{$json->{"words"}}[$startTrailer+1]->{"start"}; # 
	my $startTrailerTime = @{$json->{"words"}}[$startTrailer-1]->{"stop"}; # version that includes potential silence between words
	my $endTrailerTime = @{$json->{"words"}}[$endTrailer-1]->{"stop"};
printf "%.3f = %d/%d\t", ($withTiming / ($timeable)), $withTiming, $timeable;
print $startLeaderTime * .001 if (defined $startLeaderTime);
print "\t";
print $endLeaderTime * .001 if (defined $endLeaderTime);
print "\t";
print $startTrailerTime * .001 if (defined $startTrailerTime);
print "\t";
print $endTrailerTime * .001 if (defined $endTrailerTime);
print "\t$file\t", getReader($file), "\n";
	if (defined $startLeaderTime && defined $endLeaderTime 
	 && defined $startTrailerTime && defined $endTrailerTime) {
		makeDisclaimerAudio($file, $startLeaderTime * .001, $endLeaderTime * .001, $startTrailerTime * .001, $endTrailerTime * .001);
	} else {
		print STDERR "not making audio for $file\n";
	}
}

# guess article name from filename (this doesn't work reliably)
sub guessArticleName {
	my $file = $_[0];
	$file =~ m"([^/]*)/steps/align.data.json";
	my $name = uri_unescape($1);
#print "$name\n";
	$name =~ s/\(/\(_/g;
	$name =~ s/\)/_\)/g;
	$name =~ s/’s/_’s/g;
	$name =~ s/’n/_’n/g;
	$name =~ s/,/_,/g;
#	$name =~ s/Ã¼/ü/g; # this is weird but fixes perl breaking itself.
#	$name =~ s/Ã/Ö/g; # this is weird but fixes perl breaking itself.
#print "$name\n"; # useful for checking whether perl regexps break their own unicode
	return split /_+/, $name;
}

sub getArticleTokens {
	my $json = $_[0];
	my @words = @{$json->{"words"}};
	# we need to start looking in @words for the trailingPart only after the leadingPart plus at least a one-word title
	my $start = 1 + scalar @leadingPart;
	my $trailLength = $#trailingPart;
	my $trailJoint = join " ", @trailingPart;
	#print "trail:$trailJoint\n";
	for ( ; $start < 20; $start++) {
		my @candWords = map { $_->{"original"} } @words[$start..$start+$trailLength];
		#print "cand:", join(" ", @candWords), "\n";
		last if (join(" ", @candWords) eq $trailJoint); 
	}
	return map { $_->{"original"} } @words[$#leadingPart+1..$start-1];
}

sub alignmentJson {
	my $file = $_[0];
	local $/;
	open JSON, '<', $file or die "Could not open JSON file $file";
	binmode(JSON, ":utf8");
	my $jsonstring = <JSON>;
	close JSON;
	return decode_json(encode("utf8", $jsonstring));#from_json((join " ", <JSON>));#, { utf8 => 1 });#
}

sub getReader {
	my $file = $_[0];
	$file =~ s"/results/"/articles/";
	$file =~ s"/steps/align.data.json"/info.json";
	open JSON, '<', $file or die "Could not open JSON file $file";
	my $json = from_json((join " ", <JSON>), { utf8 => 1 });
	close JSON;
#	return $json->{audio_file}->{reader}; # old version (English data)
	return $json->{audio_file_parsed}->{reader};
}

sub makeDisclaimerAudio {
	my ($file, $startLeaderTime, $endLeaderTime, $startTrailerTime, $endTrailerTime) = @_;
	$file =~ s"/results/"/articles/";
	$file =~ s"/steps/align.data.json"/audio.wav";
	my $outfile = $file;
	$outfile =~ s"/audio.wav"/anonymizeddisclaimer.wav";
	`sox '$file' 1.wav trim $startLeaderTime =$endLeaderTime`;
	#my $articleNameTime = $startTrailerTime - $endLeaderTime;
	# version that leaves in the article name:
	#`sox '$file' -p trim $endLeaderTime =$startTrailerTime lowpass 150 | sox -m - gray_noise.wav 2.wav trim 0 $articleNameTime gain +9`;
	my $noiseTime = ($endLeaderTime - $startLeaderTime) / 2;
	`sox gray_noise.wav 2.wav trim 0 $noiseTime gain +6`;
	`sox '$file' 3.wav trim $startTrailerTime =$endTrailerTime`;
	`sox 1.wav 2.wav 3.wav '$outfile'`;
}
